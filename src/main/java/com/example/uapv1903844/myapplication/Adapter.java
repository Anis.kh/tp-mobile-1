package com.example.uapv1903844.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
public class Adapter extends ArrayAdapter<String> {

    List<String> names;
    int[] flags;
    Context mContext;

    public Adapter(Context context, List<String> countryNames, int[] countryFlags) {
        super(context, R.layout.items);
        this.names = countryNames;
        this.flags = countryFlags;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.items, parent, false);
            mViewHolder.mFlag = convertView.findViewById(R.id.img);
            mViewHolder.mName = convertView.findViewById(R.id.pays);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mFlag.setImageResource(flags[position]);
        String animal = names.get(position);
        mViewHolder.mName.setText(animal);



        return convertView;
    }
    public String getItem(int id) {
        return names.get(id);
    }
    static class ViewHolder {
        ImageView mFlag;
        TextView mName;
    }
}
