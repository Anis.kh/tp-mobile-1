package com.example.uapv1903844.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class CountryActivity extends AppCompatActivity {

    TextView nPays;
    EditText nCapitale;
    EditText nLangue;
    EditText nMonnaie;
    EditText nPopulation;
    EditText nSuperficie;
    Country country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Intent intent = getIntent();
        TextView textView = (TextView) findViewById(R.id.nPays);
        textView.setText(intent.getStringExtra("country"));
        String name = getIntent().getStringExtra("country");
        country = CountryList.getCountry(name);

        nPays = (TextView) findViewById(R.id.nPays);
        nPays.setText(name);

        ImageView image = findViewById(R.id.flag);
        int id = getResources().getIdentifier(country.getmImgFile(),"drawable",getPackageName());
        image.setImageResource(id);

        nPays = (TextView) findViewById(R.id.nPays);
        nPays.setText(name);

        nCapitale = findViewById(R.id.nCapitale);
        nCapitale.setText(country.getmCapital());

        nLangue = findViewById(R.id.nLangue);
        nLangue.setText(country.getmLanguage());

        nMonnaie = findViewById(R.id.nMonnaie);
        nMonnaie.setText(country.getmCurrency());

        nPopulation = findViewById(R.id.nPopulation);
        nPopulation.setText(country.getmPopulation()+"");

        nSuperficie = findViewById(R.id.nSuperficie);
        nSuperficie.setText(country.getmArea()+"");

        Button save = findViewById(R.id.button);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country.setmCapital(nCapitale.getText().toString());
                country.setmLanguage(nLangue.getText().toString());
                country.setmCurrency(nMonnaie.getText().toString());
                country.setmPopulation(Integer.parseInt(nPopulation.getText().toString()));
                country.setmArea(Integer.parseInt(nSuperficie.getText().toString()));
                Toast.makeText(getApplication(),"Sauvegarde réussie !",Toast.LENGTH_LONG).show();
            }
        });
    }
}

