package com.example.uapv1903844.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Adapter adapter;
    static final CountryList country1 = new CountryList();
    ListView listView;
    int[] countryFlags = {
            R.drawable.flag_of_spain,
            R.drawable.flag_of_germany,
            R.drawable.flag_of_south_africa,
            R.drawable.flag_of_the_united_states,
            R.drawable.flag_of_france,
            R.drawable.flag_of_japan};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listPays);

        Adapter adapter = new Adapter(MainActivity.this, Arrays.asList(CountryList.getNameArray()),countryFlags);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String country = parent.getAdapter().getItem(position).toString();
                Intent intent = new Intent(MainActivity.this, CountryActivity.class);
                intent.putExtra("country",country);
                startActivity(intent);
            }
        });}}



